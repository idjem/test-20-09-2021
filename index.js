const inquirer = require('inquirer');
const Promise = require('bluebird');
const redis = require('redis');
const { promisify } = require('util');

// validation
const positiveNumber = (value) => /^\+?(0|[1-9]\d*)$/.test(`${value}`);
const nameRegularExpression = (value) => /^[a-zA-ZàáâäãåąčćęèéêëėįìíîïłńòóôöõøùúûüųūÿýżźñçčšžÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎÏĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹÑßÇŒÆČŠŽ∂ð ,.'-]+$/u.test(value); //  https://stackoverflow.com/questions/2385701/regular-expression-for-first-and-last-name
const noEmpty = (value = '') => value.length;

const nElementArray = (n) => Array.from(Array(n).keys());

const validateQuestion = async (question, validation = () => true) => {
  const result = (await inquirer.prompt(question))[question.name];
  return validation(result) ? result : validateQuestion(question, validation);
};

const client = redis.createClient();
client.on('error', () => process.exit());
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const flushdbAsync = promisify(client.flushdb).bind(client);

const checkRedisAndAskQuestion = async (question, validation) => {
  const redisData = await getAsync(question.name);
  if (redisData) {
    console.log(`${question.message} ${redisData}`);
    return redisData;
  }
  const result = await validateQuestion(question, validation);
  await setAsync(question.name, result);
  return result;
};

const run = async () => {
  const truskerName = await checkRedisAndAskQuestion({ type: 'input', message: 'Trusker name :', name: 'truskerName' }, nameRegularExpression);

  const companyName = await checkRedisAndAskQuestion({ type: 'input', message: 'Company name :', name: 'companyName' }, noEmpty);

  const employeesNumber = await checkRedisAndAskQuestion({ type: 'number', message: 'Number of employees :', name: 'employeesNumber' }, positiveNumber);

  const employees = await Promise.mapSeries(
    nElementArray(parseInt(employeesNumber, 10)),
    (i) => checkRedisAndAskQuestion({ type: 'input', message: `Employe ${i + 1} name :`, name: `employe${i + 1}name` }, nameRegularExpression),
  );

  const trucksNumbers = await checkRedisAndAskQuestion({ type: 'number', message: 'Number of trucks :', name: 'trucksNumbers' }, positiveNumber);

  const trucks = await Promise.mapSeries(
    nElementArray(parseInt(trucksNumbers, 10)),
    async (i) => {
      const volume = await checkRedisAndAskQuestion({ type: 'number', message: `Truck ${i + 1} volume :`, name: `truck${i + 1}volume` }, positiveNumber);
      const type = await checkRedisAndAskQuestion({ type: 'input', message: `Trucks ${i + 1} type :`, name: `trucks${i + 1}type` }, noEmpty);
      return { volume, type };
    },
  );

  console.log({
    truskerName, companyName, employees, trucks,
  });

  const { end } = await inquirer.prompt({
    type: 'confirm', name: 'end', message: 'is this information correct?', default: false,
  });

  await flushdbAsync();

  return (!end) && run();
};

run().catch((err) => {
  console.log(err);
}).finally(() => {
  client.quit();
});
